using Godot;
using System;
using System.Collections.Generic;
using System.Threading;
using Thread = System.Threading.Thread;


public partial class Jitter : Node
{
    private Random _random;
    // private List<object> _list;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _random = new Random();
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(double delta)
    {
        for (var i = 0; i < _random.Next(10, 10000); i++)
        {
            AddChild(new Node());
        }

        if (_random.NextDouble() > 0.95)
        {
            for (int i = 0; i < GetChildCount(); i++)
            {
                GetChild(i).QueueFree();
            }
        }
        // Thread.Sleep(_random.Next(10, 100));
    }
}