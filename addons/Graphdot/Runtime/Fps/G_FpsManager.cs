using System;
using System.Collections.Generic;
using System.Linq;
using Godot;
using Graphdot.Runtime.Util;

namespace Graphdot.Runtime.Fps
{
	public partial class G_FpsManager : Node //, IMovable, IModifiableState
	{
		#region Variables -> Serialized Private

		[Export]
		private Node[] m_nonBasicTextGameObjects = Array.Empty<Node>();

		[Export]
		private Panel[] m_backgroundImages = Array.Empty<Panel>();

		#endregion

		#region Variables -> Private

		private GraphdotManager m_GraphdotManager = null;

		private G_FpsGraph m_fpsGraph = null;
		private G_FpsMonitor m_fpsMonitor = null;
		private G_FpsText m_fpsText = null;

		private Control m_rectTransform = null;

		private GraphdotManager.ModuleState m_previousModuleState = GraphdotManager.ModuleState.FULL;
		private GraphdotManager.ModuleState m_currentModuleState = GraphdotManager.ModuleState.FULL;

		#endregion

		#region Methods -> Unity Callbacks

		public override void _EnterTree()
		{
			Init();
		}

		public override void _Ready()
		{
			UpdateParameters();
		}

		#endregion

		#region Methods -> Public

		public void SetPosition(GraphdotManager.ModulePosition newModulePosition)
		{
			float xSideOffset = Mathf.Abs(m_rectTransform.AnchorLeft);
			float ySideOffset = Mathf.Abs(m_rectTransform.AnchorTop);

			switch (newModulePosition)
			{
				case GraphdotManager.ModulePosition.TOP_LEFT:

					// m_rectTransform.anchm = Vector2.up;
					// m_rectTransform.anchorMin = Vector2.up;
					// m_rectTransform.anchoredPosition = new Vector2(xSideOffset, -ySideOffset);

					break;

				case GraphdotManager.ModulePosition.TOP_RIGHT:

					// m_rectTransform.anchorMax = Vector2.one;
					// m_rectTransform.anchorMin = Vector2.one;
					// m_rectTransform.anchoredPosition = new Vector2(-xSideOffset, -ySideOffset);

					break;

				case GraphdotManager.ModulePosition.BOTTOM_LEFT:

					// m_rectTransform.anchorMax = Vector2.zero;
					// m_rectTransform.anchorMin = Vector2.zero;
					// m_rectTransform.anchoredPosition = new Vector2(xSideOffset, ySideOffset);

					break;

				case GraphdotManager.ModulePosition.BOTTOM_RIGHT:

					// m_rectTransform.anchorMax = Vector2.right;
					// m_rectTransform.anchorMin = Vector2.right;
					// m_rectTransform.anchoredPosition = new Vector2(-xSideOffset, ySideOffset);

					break;

				case GraphdotManager.ModulePosition.FREE:
					break;
			}
		}

		public void SetState(GraphdotManager.ModuleState state, bool silentUpdate = false)
		{
			if (!silentUpdate)
			{
				m_previousModuleState = m_currentModuleState;
			}

			m_currentModuleState = state;

			switch (state)
			{
				case GraphdotManager.ModuleState.FULL:
					SetProcess(true); //setactive
					// GetChildren().SetAllActive(true);
					SetGraphActive(true);

					// if (m_graphdotManager.Background)
					// {
					//     m_backgroundImages.SetOneActive(0);
					// }
					// else
					// {
					//     m_backgroundImages.SetAllActive(false);
					// }

					break;

				case GraphdotManager.ModuleState.TEXT:
					SetProcess(true);
					// m_childrenGameObjects.SetAllActive(true);
					SetGraphActive(false);

					// if (m_graphdotManager.Background)
					// {
					//     m_backgroundImages.SetOneActive(1);
					// }
					// else
					// {
					//     m_backgroundImages.SetAllActive(false);
					// }

					break;

				case GraphdotManager.ModuleState.BASIC:
					SetProcess(true);
					// m_childrenGameObjects.SetAllActive(true);
					bool all = m_nonBasicTextGameObjects.SetAllActive(false).All(_ => true);
					SetGraphActive(false);
					//
					// if (m_graphdotManager.Background)
					// {
					//     m_backgroundImages.SetOneActive(2);
					// }
					// else
					// {
					//     m_backgroundImages.SetAllActive(false);
					// }

					break;

				case GraphdotManager.ModuleState.BACKGROUND:
					SetProcess(true);
					// m_childrenGameObjects.SetAllActive(false);
					SetGraphActive(false);

					// m_backgroundImages.SetAllActive(false);
					break;

				case GraphdotManager.ModuleState.OFF:
					SetProcess(false);
					break;
			}
		}

		public void RestorePreviousState()
		{
			SetState(m_previousModuleState);
		}

		public void UpdateParameters()
		{
			foreach (var image in m_backgroundImages)
			{
				image.AddThemeColorOverride("Background", m_GraphdotManager.BackgroundColor);
			}

			m_fpsGraph.UpdateParameters();
			m_fpsMonitor.UpdateParameters();
			m_fpsText.UpdateParameters();

			SetState(m_GraphdotManager.FpsModuleState);
		}

		public void RefreshParameters()
		{
			foreach (var image in m_backgroundImages)
			{
				image.AddThemeColorOverride("Background", m_GraphdotManager.BackgroundColor);
			}

			m_fpsGraph.UpdateParameters();
			m_fpsMonitor.UpdateParameters();
			m_fpsText.UpdateParameters();

			SetState(m_currentModuleState, true);
		}

		#endregion

		#region Methods -> Private

		private void Init()
		{
			m_GraphdotManager = GetTree().Root.GetNode<GraphdotManager>();

			m_rectTransform = this.GetNode<Control>();

			m_fpsMonitor = this.GetNode<G_FpsMonitor>();
			m_fpsGraph = this.GetNode<G_FpsGraph>();
			m_fpsGraph.FPSMonitor = m_fpsMonitor;
			m_fpsText = this.GetNode<G_FpsText>();
			m_fpsText.GraphdotManager = m_GraphdotManager;
		}

		private void SetGraphActive(bool active)
		{
			m_fpsGraph.SetProcess(active);
		}

		#endregion
	}
}
