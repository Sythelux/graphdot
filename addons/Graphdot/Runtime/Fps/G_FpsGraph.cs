using Godot;
using System;
using Graphdot.Runtime;
using Graphdot.Runtime.Shaders;
using Graphdot.Runtime.Util;

namespace Graphdot.Runtime.Fps
{
	public partial class G_FpsGraph : TextureRect
	{
		#region Variables -> Serialized Private

		[Export]
		private Shader ShaderFull = null;

		[Export]
		private Shader ShaderLight = null;

		// This keeps track of whether Init() has run or not
		[Export]
		private bool m_isInitialized = false;

		#endregion

		#region Variables -> Private

		private GraphdotManager m_graphdotManager = null;

		private G_FpsMonitor m_fpsMonitor = null;

		private int m_resolution = 150;

		private G_GraphShader m_shaderGraph = null;

		private int[] m_fpsArray;

		private int m_highestFps;

		public G_FpsMonitor FPSMonitor
		{
			get => m_fpsMonitor;
			set => m_fpsMonitor = value;
		}

		#endregion

		#region Methods -> Unity Callbacks

		public override void _Process(double delta)
		{
			UpdateGraph();
		}

		#endregion

		#region Methods -> Public

		public void UpdateParameters()
		{
			if (m_shaderGraph == null)
			{
				// TODO: While Graphdot is disabled (e.g. by default via Ctrl+H) and while in Editor after a Hot-Swap,
				// the OnApplicationFocus calls this while m_shaderGraph == null, throwing a NullReferenceException
				return;
			}

			switch (m_graphdotManager.GraphdotMode)
			{
				case GraphdotManager.Mode.FULL:
					m_shaderGraph.ArrayMaxSize = G_GraphShader.ArrayMaxSizeFull;
					// m_shaderGraph.Image.Material = new Material(ShaderFull);
					break;

				case GraphdotManager.Mode.LIGHT:
					m_shaderGraph.ArrayMaxSize = G_GraphShader.ArrayMaxSizeLight;
					// m_shaderGraph.Image.material = new Material(ShaderLight);
					break;
			}

			m_shaderGraph.InitializeShader();

			m_resolution = m_graphdotManager.FpsGraphResolution;

			CreatePoints();
		}

		#endregion

		#region Methods -> Protected Override

		protected void UpdateGraph()
		{
			// Since we no longer initialize by default OnEnable(), 
			// we need to check here, and Init() if needed
			if (!m_isInitialized)
			{
				Init();
			}

			short fps = (short)(1 / GetProcessDeltaTime());

			int currentMaxFps = 0;

			for (int i = 0; i <= m_resolution - 1; i++)
			{
				if (i >= m_resolution - 1)
				{
					m_fpsArray[i] = fps;
				}
				else
				{
					m_fpsArray[i] = m_fpsArray[i + 1];
				}

				// Store the highest fps to use as the highest point in the graph

				if (currentMaxFps < m_fpsArray[i])
				{
					currentMaxFps = m_fpsArray[i];
				}
			}

			m_highestFps = m_highestFps < 1 || m_highestFps <= currentMaxFps ? currentMaxFps : m_highestFps - 1;

			m_highestFps = m_highestFps > 0 ? m_highestFps : 1;

			if (m_shaderGraph.ShaderArrayValues == null)
			{
				m_fpsArray = new int[m_resolution];
				m_shaderGraph.ShaderArrayValues = new float[m_resolution];
			}

			for (int i = 0; i <= m_resolution - 1; i++)
			{
				m_shaderGraph.ShaderArrayValues[i] = m_fpsArray[i] / (float)m_highestFps;
			}

			// Update the material values

			m_shaderGraph.UpdatePoints();

			m_shaderGraph.Average = (float)m_fpsMonitor.AverageFPS / m_highestFps;
			m_shaderGraph.UpdateAverage();

			m_shaderGraph.GoodThreshold = (float)m_graphdotManager.GoodFPSThreshold / m_highestFps;
			m_shaderGraph.CautionThreshold = (float)m_graphdotManager.CautionFPSThreshold / m_highestFps;
			m_shaderGraph.UpdateThresholds();
		}

		protected void CreatePoints()
		{
			if (m_shaderGraph.ShaderArrayValues == null || m_fpsArray.Length != m_resolution)
			{
				m_fpsArray = new int[m_resolution];
				m_shaderGraph.ShaderArrayValues = new float[m_resolution];
			}

			for (int i = 0; i < m_resolution; i++)
			{
				m_shaderGraph.ShaderArrayValues[i] = 0;
			}

			m_shaderGraph.GoodColor = m_graphdotManager.GoodFPSColor;
			m_shaderGraph.CautionColor = m_graphdotManager.CautionFPSColor;
			m_shaderGraph.CriticalColor = m_graphdotManager.CriticalFPSColor;

			m_shaderGraph.UpdateColors();

			m_shaderGraph.UpdateArray();
		}

		#endregion

		#region Methods -> Private

		private void Init()
		{
			m_graphdotManager = GetTree().Root.GetNode<GraphdotManager>();

			m_shaderGraph = new G_GraphShader
			{
				Image = this
			};

			UpdateParameters();

			m_isInitialized = true;
		}

		#endregion
	}
}
