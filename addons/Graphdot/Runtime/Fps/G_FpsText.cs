using Godot;
using Graphdot.Runtime;
using Graphdot.Runtime.Util;

namespace Graphdot.Runtime.Fps
{
    public partial class G_FpsText : Control
    {
        #region Variables -> Serialized Private

        [Export]
        private RichTextLabel m_fpsText = null;

        [Export]
        private RichTextLabel m_msText = null;

        [Export]
        private RichTextLabel m_avgFpsText = null;

        [Export]
        private RichTextLabel m_onePercentFpsText = null;

        [Export]
        private RichTextLabel m_zero1PercentFpsText = null;

        #endregion

        #region Variables -> Private

        private GraphdotManager m_GraphdotManager = null;

        private G_FpsMonitor m_fpsMonitor = null;

        private int m_updateRate = 4; // 4 updates per sec.

        private int m_frameCount = 0;

        private double m_deltaTime = 0f;

        private float m_fps = 0f;

        private double m_ms = 0f;

        private const string m_msStringFormat = "0.0";

        public GraphdotManager GraphdotManager
        {
            get => m_GraphdotManager;
            set => m_GraphdotManager = value;
        }

        #endregion

        #region Methods -> Unity Callbacks

        public override void _Ready()
        {
            Init();
        }

        public override void _Process(double delta)
        {
            m_deltaTime += delta; //TODO: unscaled time

            m_frameCount++;

            // Only update texts 'm_updateRate' times per second

            if (m_deltaTime > 1f / m_updateRate)
            {
                m_fps = (float)(m_frameCount / m_deltaTime);
                m_ms = m_deltaTime / m_frameCount * 1000f;

                // Update fps
                m_fpsText.Text = Mathf.RoundToInt(m_fps).ToStringNonAlloc();
                SetFpsRelatedTextColor(m_fpsText, m_fps);

                // Update ms
                m_msText.Text = m_ms.ToString(m_msStringFormat);
                SetFpsRelatedTextColor(m_msText, m_fps);

                // Update 1% fps
                m_onePercentFpsText.Text = ((int)(m_fpsMonitor.OnePercentFPS)).ToStringNonAlloc();
                SetFpsRelatedTextColor(m_onePercentFpsText, m_fpsMonitor.OnePercentFPS);

                // Update 0.1% fps
                m_zero1PercentFpsText.Text = ((int)(m_fpsMonitor.Zero1PercentFps)).ToStringNonAlloc();
                SetFpsRelatedTextColor(m_zero1PercentFpsText, m_fpsMonitor.Zero1PercentFps);

                // Update avg fps
                m_avgFpsText.Text = ((int)(m_fpsMonitor.AverageFPS)).ToStringNonAlloc();
                SetFpsRelatedTextColor(m_avgFpsText, m_fpsMonitor.AverageFPS);

                // Reset variables
                m_deltaTime = 0f;
                m_frameCount = 0;
            }
        }

        #endregion

        #region Methods -> Public

        public void UpdateParameters()
        {
            m_updateRate = m_GraphdotManager.FpsTextUpdateRate;
        }

        #endregion

        #region Methods -> Private

        /// <summary>
        /// Assigns color to a text according to their fps numeric value and
        /// the colors specified in the 3 categories (Good, Caution, Critical).
        /// </summary>
        /// 
        /// <param name="text">
        /// UI Text component to change its color
        /// </param>
        /// 
        /// <param name="fps">
        /// Numeric fps value
        /// </param>
        private void SetFpsRelatedTextColor(RichTextLabel text, float fps)
        {
            int roundedFps = Mathf.RoundToInt(fps);

            if (roundedFps >= m_GraphdotManager.GoodFPSThreshold)
            {
                text.Set("custom_colors/default_color", m_GraphdotManager.GoodFPSColor);
            }
            else if (roundedFps >= m_GraphdotManager.CautionFPSThreshold)
            {
                text.Set("custom_colors/default_color", m_GraphdotManager.CautionFPSColor);
            }
            else
            {
                text.Set("custom_colors/default_color", m_GraphdotManager.CriticalFPSColor);
            }
        }

        private void Init()
        {
            G_IntString.Init(0, 2000); // Max fps expected
            G_FloatString.Init(0, 100); // Max ms expected per frame

            m_fpsMonitor = GetParent().GetParent().GetNode<G_FpsMonitor>();
            m_fpsText = G_ExtensionMethods.GetNode<RichTextLabel>(this, "fps_text_value");
            m_msText = G_ExtensionMethods.GetNode<RichTextLabel>(this, "ms_text_value");
            m_avgFpsText = G_ExtensionMethods.GetNode<RichTextLabel>(this, "avg_fps_text_value");
            m_onePercentFpsText = G_ExtensionMethods.GetNode<RichTextLabel>(this, "1%_fps_text_value");
            m_zero1PercentFpsText = G_ExtensionMethods.GetNode<RichTextLabel>(this, "01%_fps_text_value");

            UpdateParameters();
        }

        #endregion
    }
}