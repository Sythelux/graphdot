using System;
using Godot;
using Graphdot.Runtime.Audio;
using Graphdot.Runtime.Fps;
using Graphdot.Runtime.Ram;
using Graphdot.Runtime.Util;

namespace Graphdot.Runtime
{
    public partial class GraphdotManager : G_Singleton<GraphdotManager>
    {
        protected GraphdotManager()
        {
        }

        //Enums

        #region Enums -> Public

        public enum Mode
        {
            FULL = 0,
            LIGHT = 1
        }

        public enum ModuleType
        {
            FPS = 0,
            RAM = 1,
            AUDIO = 2,
            ADVANCED = 3
        }

        public enum ModuleState
        {
            FULL = 0,
            TEXT = 1,
            BASIC = 2,
            BACKGROUND = 3,
            OFF = 4
        }

        public enum ModulePosition
        {
            TOP_RIGHT = 0,
            TOP_LEFT = 1,
            BOTTOM_RIGHT = 2,
            BOTTOM_LEFT = 3,
            FREE = 4
        }

        public enum LookForAudioListener
        {
            ALWAYS,
            ON_SCENE_LOAD,
            NEVER
        }

        public enum ModulePreset
        {
            FPS_BASIC = 0,
            FPS_TEXT = 1,
            FPS_FULL = 2,

            FPS_TEXT_RAM_TEXT = 3,
            FPS_FULL_RAM_TEXT = 4,
            FPS_FULL_RAM_FULL = 5,

            FPS_TEXT_RAM_TEXT_AUDIO_TEXT = 6,
            FPS_FULL_RAM_TEXT_AUDIO_TEXT = 7,
            FPS_FULL_RAM_FULL_AUDIO_TEXT = 8,
            FPS_FULL_RAM_FULL_AUDIO_FULL = 9,

            FPS_FULL_RAM_FULL_AUDIO_FULL_ADVANCED_FULL = 10,
            FPS_BASIC_ADVANCED_FULL = 11
        }

        #endregion

        #region Variables -> Serialized Private

        [Export]
        private Mode m_graphdotMode = Mode.FULL;

        [Export]
        private bool m_enableOnStartup = true;

        [Export]
        private bool m_keepAlive = true;

        [Export]
        private bool m_background = true;

        [Export]
        private Color m_backgroundColor = new Color(0, 0, 0, 0.3f);

        [Export]
        private bool m_enableHotkeys = true;

        [Export]
        private Key m_toggleModeKeyCode = Key.G;

        [Export]
        private bool m_toggleModeCtrl = true;

        [Export]
        private bool m_toggleModeAlt = false;

        [Export]
        private Key m_toggleActiveKeyCode = Key.H;

        [Export]
        private bool m_toggleActiveCtrl = true;

        [Export]
        private bool m_toggleActiveAlt = false;

        [Export]
        private ModulePosition m_graphModulePosition = ModulePosition.TOP_RIGHT;

        // Fps ---------------------------------------------------------------------------

        [Export]
        private ModuleState m_fpsModuleState = ModuleState.FULL;

        [Export]
        private Color m_goodFpsColor = new Color(118 / 255f, 212 / 255f, 58 / 255f, 255 / 255f);

        [Export]
        private int m_goodFpsThreshold = 60;

        [Export]
        private Color m_cautionFpsColor = new Color(243 / 255f, 232 / 255f, 0 / 255f, 255 / 255f);

        [Export]
        private int m_cautionFpsThreshold = 30;

        [Export]
        private Color m_criticalFpsColor = new Color(220 / 255f, 41 / 255f, 30 / 255f, 255 / 255f);

        // [Range(10, 300)]
        [Export]
        private int m_fpsGraphResolution = 150;

        // [Range(1, 200)]
        [Export]
        private int m_fpsTextUpdateRate = 3; // 3 updates per sec.

        // Ram ---------------------------------------------------------------------------

        [Export]
        private ModuleState m_ramModuleState = ModuleState.FULL;

        [Export]
        private Color m_staticMemoryUsageColor = new Color(255 / 255f, 190 / 255f, 60 / 255f, 255 / 255f);

        [Export]
        private Color m_dynamicMemoryUsageColor = new Color(205 / 255f, 84 / 255f, 229 / 255f, 255 / 255f);

        [Export]
        private Color m_staticMemoryPeakUsageColor = new Color(0.3f, 0.65f, 1f, 1);

        // [Range(10, 300)]
        [Export]
        private int m_ramGraphResolution = 150;


        // [Range(1, 200)]
        [Export]
        private int m_ramTextUpdateRate = 3; // 3 updates per sec.

        // Audio -------------------------------------------------------------------------

        [Export]
        private ModuleState m_audioModuleState = ModuleState.FULL;

        [Export]
        private LookForAudioListener m_findAudioListenerInCameraIfNull = LookForAudioListener.ON_SCENE_LOAD;

        [Export]
        private AudioEffectSpectrumAnalyzerInstance m_audioListener = null;

        [Export]
        private Color m_audioGraphColor = Colors.White;

        // [Range(10, 300)]
        [Export]
        private int m_audioGraphResolution = 81;

        // [Range(1, 200)]
        [Export]
        private int m_audioTextUpdateRate = 3; // 3 updates per sec.

        // [Export]
        private FFTWindow m_FFTWindow = FFTWindow.Blackman;

        // [Tooltip("Must be a power of 2 and between 64-8192")]
        [Export]
        private int m_spectrumSize = 512;

        // Advanced ----------------------------------------------------------------------

        [Export]
        private ModulePosition m_advancedModulePosition = ModulePosition.BOTTOM_LEFT;

        [Export]
        private ModuleState m_advancedModuleState = ModuleState.FULL;

        #endregion

        #region Variables -> Private

        private bool m_initialized = false;
        private bool m_active = true;
        private bool m_focused = true;

        private G_FpsManager m_fpsManager = null;
        private G_RamManager m_ramManager = null;

        private G_AudioManager m_audioManager = null;
        // private G_AdvancedData m_advancedData = null;

        private G_FpsMonitor m_fpsMonitor = null;
        private G_RamMonitor m_ramMonitor = null;
        private G_AudioMonitor m_audioMonitor = null;

        private ModulePreset m_modulePresetState = ModulePreset.FPS_BASIC_ADVANCED_FULL;

        #endregion

        //TODO: Maybe sort these into Get and GetSet sections.

        #region Properties -> Public

        public Mode GraphdotMode
        {
            get { return m_graphdotMode; }
            set
            {
                m_graphdotMode = value;
                UpdateAllParameters();
            }
        }

        public bool EnableOnStartup
        {
            get { return m_enableOnStartup; }
        }

        public bool KeepAlive
        {
            get { return m_keepAlive; }
        }

        public bool Background
        {
            get { return m_background; }
            set
            {
                m_background = value;
                UpdateAllParameters();
            }
        }

        public Color BackgroundColor
        {
            get { return m_backgroundColor; }
            set
            {
                m_backgroundColor = value;
                UpdateAllParameters();
            }
        }

        public ModulePosition GraphModulePosition
        {
            get { return m_graphModulePosition; }
            set
            {
                m_graphModulePosition = value;
                m_fpsManager.SetPosition(m_graphModulePosition);
                m_ramManager.SetPosition(m_graphModulePosition);
                // m_audioManager.SetPosition(m_graphModulePosition);
            }
        }

        // Fps ---------------------------------------------------------------------------

        // Setters & Getters

        public ModuleState FpsModuleState
        {
            get { return m_fpsModuleState; }
            set
            {
                m_fpsModuleState = value;
                m_fpsManager.SetState(m_fpsModuleState);
            }
        }

        public Color GoodFPSColor
        {
            get { return m_goodFpsColor; }
            set
            {
                m_goodFpsColor = value;
                m_fpsManager.UpdateParameters();
            }
        }

        public Color CautionFPSColor
        {
            get { return m_cautionFpsColor; }
            set
            {
                m_cautionFpsColor = value;
                m_fpsManager.UpdateParameters();
            }
        }

        public Color CriticalFPSColor
        {
            get { return m_criticalFpsColor; }
            set
            {
                m_criticalFpsColor = value;
                m_fpsManager.UpdateParameters();
            }
        }

        public int GoodFPSThreshold
        {
            get { return m_goodFpsThreshold; }
            set
            {
                m_goodFpsThreshold = value;
                m_fpsManager.UpdateParameters();
            }
        }

        public int CautionFPSThreshold
        {
            get { return m_cautionFpsThreshold; }
            set
            {
                m_cautionFpsThreshold = value;
                m_fpsManager.UpdateParameters();
            }
        }

        public int FpsGraphResolution
        {
            get { return m_fpsGraphResolution; }
            set
            {
                m_fpsGraphResolution = value;
                m_fpsManager.UpdateParameters();
            }
        }

        public int FpsTextUpdateRate
        {
            get { return m_fpsTextUpdateRate; }
            set
            {
                m_fpsTextUpdateRate = value;
                m_fpsManager.UpdateParameters();
            }
        }

        // Getters

        public float CurrentFPS
        {
            get { return m_fpsMonitor.CurrentFPS; }
        }

        public float AverageFPS
        {
            get { return m_fpsMonitor.AverageFPS; }
        }

        public float MinFPS
        {
            get { return m_fpsMonitor.OnePercentFPS; }
        }

        public float MaxFPS
        {
            get { return m_fpsMonitor.Zero1PercentFps; }
        }

        // Ram ---------------------------------------------------------------------------

        // Setters & Getters

        public ModuleState RamModuleState
        {
            get { return m_ramModuleState; }
            set
            {
                m_ramModuleState = value;
                m_ramManager.SetState(m_ramModuleState);
            }
        }


        public Color StaticMemoryUsageColor
        {
            get { return m_staticMemoryUsageColor; }
            set
            {
                m_staticMemoryUsageColor = value;
                m_ramManager.UpdateParameters();
            }
        }

        public Color DynamicMemoryUsageColor
        {
            get { return m_dynamicMemoryUsageColor; }
            set
            {
                m_dynamicMemoryUsageColor = value;
                m_ramManager.UpdateParameters();
            }
        }

        public Color StaticMemoryPeakUsageColor
        {
            get { return m_staticMemoryPeakUsageColor; }
            set
            {
                m_staticMemoryPeakUsageColor = value;
                m_ramManager.UpdateParameters();
            }
        }

        public int RamGraphResolution
        {
            get { return m_ramGraphResolution; }
            set
            {
                m_ramGraphResolution = value;
                m_ramManager.UpdateParameters();
            }
        }

        public int RamTextUpdateRate
        {
            get { return m_ramTextUpdateRate; }
            set
            {
                m_ramTextUpdateRate = value;
                m_ramManager.UpdateParameters();
            }
        }

        // Getters

        public float StaticMemoryUsage => m_ramMonitor.StaticMemoryUsage;

        public float DynamicMemoryUsage => m_ramMonitor.DynamicMemoryUsage;

        public float StaticMemoryPeakUsage => m_ramMonitor.StaticMemoryPeakUsage;

        // Audio -------------------------------------------------------------------------

        // Setters & Getters

        public ModuleState AudioModuleState
        {
            get { return m_audioModuleState; }
            set
            {
                m_audioModuleState = value;
                m_audioManager.SetState(m_audioModuleState);
            }
        }

        public AudioEffectSpectrumAnalyzerInstance AudioListener
        {
            get { return m_audioListener; }
            set
            {
                m_audioListener = value;
                m_audioManager.UpdateParameters();
            }
        }

        public LookForAudioListener FindAudioListenerInCameraIfNull
        {
            get { return m_findAudioListenerInCameraIfNull; }
            set
            {
                m_findAudioListenerInCameraIfNull = value;
                // m_audioManager.UpdateParameters();
            }
        }

        public Color AudioGraphColor
        {
            get { return m_audioGraphColor; }
            set
            {
                m_audioGraphColor = value;
                m_audioManager.UpdateParameters();
            }
        }

        public int AudioGraphResolution
        {
            get { return m_audioGraphResolution; }
            set
            {
                m_audioGraphResolution = value;
                m_audioManager.UpdateParameters();
            }
        }

        public int AudioTextUpdateRate
        {
            get { return m_audioTextUpdateRate; }
            set
            {
                m_audioTextUpdateRate = value;
                m_audioManager.UpdateParameters();
            }
        }

        public FFTWindow FftWindow
        {
            get { return m_FFTWindow; }
            set
            {
                m_FFTWindow = value;
                m_audioManager.UpdateParameters();
            }
        }

        public int SpectrumSize
        {
            get { return m_spectrumSize; }
            set
            {
                m_spectrumSize = value;
                m_audioManager.UpdateParameters();
            }
        }

        // Getters

        /// <summary>
        /// Current audio spectrum from the specified AudioListener.
        /// </summary>
        public float[] Spectrum => m_audioMonitor.Spectrum;

        /// <summary>
        /// Maximum DB registered in the current spectrum.
        /// </summary>
        public float MaxDB => m_audioMonitor.MaxDB;


        // Advanced ---------------------------------------------------------------------

        // Setters & Getters

        public ModuleState AdvancedModuleState
        {
            get { return m_advancedModuleState; }
            set
            {
                m_advancedModuleState = value;
                // m_advancedData.SetState(m_advancedModuleState);
            }
        }

        public ModulePosition AdvancedModulePosition
        {
            get { return m_advancedModulePosition; }
            set
            {
                m_advancedModulePosition = value;
                // m_advancedData.SetPosition(m_advancedModulePosition);
            }
        }

        #endregion

        #region Methods -> Unity Callbacks

        public override void _Ready()
        {
            Init();
        }

        protected override void Dispose(bool disposing)
        {
            G_IntString.Dispose();
            G_FloatString.Dispose();
        }

        private void OnApplicationFocus(bool isFocused)
        {
            m_focused = isFocused;

            if (m_initialized && isFocused)
            {
                RefreshAllParameters();
            }
        }

        #endregion

        #region Methods -> Public

        public void SetModulePosition(ModuleType moduleType, ModulePosition modulePosition)
        {
            switch (moduleType)
            {
                case ModuleType.FPS:
                case ModuleType.RAM:
                case ModuleType.AUDIO:
                    m_graphModulePosition = modulePosition;

                    m_ramManager.SetPosition(modulePosition);
                    m_fpsManager.SetPosition(modulePosition);
                    m_audioManager.SetPosition(modulePosition);
                    break;

                case ModuleType.ADVANCED:
                    // m_advancedData.SetPosition(modulePosition);
                    break;
            }
        }

        public void SetModuleMode(ModuleType moduleType, ModuleState moduleState)
        {
            switch (moduleType)
            {
                case ModuleType.FPS:
                    m_fpsManager.SetState(moduleState);
                    break;

                case ModuleType.RAM:
                    m_ramManager.SetState(moduleState);
                    break;

                case ModuleType.AUDIO:
                    // m_audioManager.SetState(moduleState);
                    break;

                case ModuleType.ADVANCED:
                    // m_advancedData.SetState(moduleState);
                    break;
            }
        }

        public void ToggleModes()
        {
            if ((int)m_modulePresetState >= Enum.GetNames(typeof(ModulePreset)).Length - 1)
            {
                m_modulePresetState = 0;
            }
            else
            {
                m_modulePresetState++;
            }

            SetPreset(m_modulePresetState);
        }

        public void SetPreset(ModulePreset modulePreset)
        {
            m_modulePresetState = modulePreset;

            switch (m_modulePresetState)
            {
                case ModulePreset.FPS_BASIC:
                    m_fpsManager.SetState(ModuleState.BASIC);
                    m_ramManager.SetState(ModuleState.OFF);
                    // m_audioManager.SetState(ModuleState.OFF);
                    // m_advancedData.SetState(ModuleState.OFF);
                    break;

                case ModulePreset.FPS_TEXT:
                    m_fpsManager.SetState(ModuleState.TEXT);
                    m_ramManager.SetState(ModuleState.OFF);
                    // m_audioManager.SetState(ModuleState.OFF);
                    // m_advancedData.SetState(ModuleState.OFF);
                    break;

                case ModulePreset.FPS_FULL:
                    m_fpsManager.SetState(ModuleState.FULL);
                    m_ramManager.SetState(ModuleState.OFF);
                    // m_audioManager.SetState(ModuleState.OFF);
                    // m_advancedData.SetState(ModuleState.OFF);
                    break;

                case ModulePreset.FPS_TEXT_RAM_TEXT:
                    m_fpsManager.SetState(ModuleState.TEXT);
                    m_ramManager.SetState(ModuleState.TEXT);
                    // m_audioManager.SetState(ModuleState.OFF);
                    // m_advancedData.SetState(ModuleState.OFF);
                    break;

                case ModulePreset.FPS_FULL_RAM_TEXT:
                    m_fpsManager.SetState(ModuleState.FULL);
                    m_ramManager.SetState(ModuleState.TEXT);
                    // m_audioManager.SetState(ModuleState.OFF);
                    // m_advancedData.SetState(ModuleState.OFF);
                    break;

                case ModulePreset.FPS_FULL_RAM_FULL:
                    m_fpsManager.SetState(ModuleState.FULL);
                    m_ramManager.SetState(ModuleState.FULL);
                    // m_audioManager.SetState(ModuleState.OFF);
                    // m_advancedData.SetState(ModuleState.OFF);
                    break;

                case ModulePreset.FPS_TEXT_RAM_TEXT_AUDIO_TEXT:
                    m_fpsManager.SetState(ModuleState.TEXT);
                    m_ramManager.SetState(ModuleState.TEXT);
                    // m_audioManager.SetState(ModuleState.TEXT);
                    // m_advancedData.SetState(ModuleState.OFF);
                    break;

                case ModulePreset.FPS_FULL_RAM_TEXT_AUDIO_TEXT:
                    m_fpsManager.SetState(ModuleState.FULL);
                    m_ramManager.SetState(ModuleState.TEXT);
                    // m_audioManager.SetState(ModuleState.TEXT);
                    // m_advancedData.SetState(ModuleState.OFF);
                    break;

                case ModulePreset.FPS_FULL_RAM_FULL_AUDIO_TEXT:
                    m_fpsManager.SetState(ModuleState.FULL);
                    m_ramManager.SetState(ModuleState.FULL);
                    // m_audioManager.SetState(ModuleState.TEXT);
                    // m_advancedData.SetState(ModuleState.OFF);
                    break;

                case ModulePreset.FPS_FULL_RAM_FULL_AUDIO_FULL:
                    m_fpsManager.SetState(ModuleState.FULL);
                    m_ramManager.SetState(ModuleState.FULL);
                    // m_audioManager.SetState(ModuleState.FULL);
                    // m_advancedData.SetState(ModuleState.OFF);
                    break;

                case ModulePreset.FPS_FULL_RAM_FULL_AUDIO_FULL_ADVANCED_FULL:
                    m_fpsManager.SetState(ModuleState.FULL);
                    m_ramManager.SetState(ModuleState.FULL);
                    // m_audioManager.SetState(ModuleState.FULL);
                    // m_advancedData.SetState(ModuleState.FULL);
                    break;

                case ModulePreset.FPS_BASIC_ADVANCED_FULL:
                    m_fpsManager.SetState(ModuleState.BASIC);
                    m_ramManager.SetState(ModuleState.OFF);
                    // m_audioManager.SetState(ModuleState.OFF);
                    // m_advancedData.SetState(ModuleState.FULL);
                    break;

                default:
                    Console.WriteLine("[GraphdotManager]::SetPreset - Tried to set a preset that is not supported.");
                    break;
            }
        }

        public void ToggleActive()
        {
            if (!m_active)
            {
                Enable();
            }
            else
            {
                Disable();
            }
        }

        public void Enable()
        {
            if (!m_active)
            {
                if (m_initialized)
                {
                    m_fpsManager.RestorePreviousState();
                    m_ramManager.RestorePreviousState();
                    // m_audioManager.RestorePreviousState();
                    // m_advancedData.RestorePreviousState();

                    m_active = true;
                }
                else
                {
                    Init();
                }
            }
        }

        public void Disable()
        {
            if (m_active)
            {
                m_fpsManager.SetState(ModuleState.OFF);
                m_ramManager.SetState(ModuleState.OFF);
                // m_audioManager.SetState(ModuleState.OFF);
                // m_advancedData.SetState(ModuleState.OFF);

                m_active = false;
            }
        }

        #endregion

        #region Methods -> Private

        private void Init()
        {
            if (m_keepAlive)
            {
                // DontDestroyOnLoad(transform.root.gameObject);
            }

            m_fpsMonitor = this.GetNode<G_FpsMonitor>();
            // m_ramMonitor = GetComponentInChildren(typeof(G_RamMonitor), true) as G_RamMonitor;
            // m_audioMonitor = GetComponentInChildren(typeof(G_AudioMonitor), true) as G_AudioMonitor;

            m_fpsManager = this.GetNode<G_FpsManager>();
            m_ramManager = this.GetNode<G_RamManager>();
            // m_audioManager = GetComponentInChildren(typeof(G_AudioManager), true) as G_AudioManager;
            // m_advancedData = GetComponentInChildren(typeof(G_AdvancedData), true) as G_AdvancedData;

            m_fpsManager.SetPosition(m_graphModulePosition);
            m_ramManager.SetPosition(m_graphModulePosition);
            // m_audioManager.SetPosition(m_graphModulePosition);
            // m_advancedData.SetPosition(m_advancedModulePosition);

            m_fpsManager.SetState(m_fpsModuleState);
            m_ramManager.SetState(m_ramModuleState);
            // m_audioManager.SetState(m_audioModuleState);
            // m_advancedData.SetState(m_advancedModuleState);

            if (!m_enableOnStartup)
            {
                ToggleActive();

                // We need to enable this on startup because we disable it in GraphdotManagerEditor
                GetNode<Control>(".").SetProcess(true);
            }

            m_initialized = true;
        }

        public override void _Input(InputEvent inputEvent)
        {
            // Toggle Mode ---------------------------------------
            if (!m_focused || !m_enableHotkeys)
                return;
            if (inputEvent is InputEventKey keyEvent && keyEvent.Pressed)
            {
                if (m_toggleModeCtrl && m_toggleModeAlt)
                {
                    if (keyEvent.Keycode == m_toggleModeKeyCode && keyEvent.AltPressed && keyEvent.IsCommandOrControlPressed())
                        ToggleModes();
                }
                else if (m_toggleModeCtrl)
                {
                    if (keyEvent.Keycode == m_toggleModeKeyCode && keyEvent.IsCommandOrControlPressed())
                        ToggleModes();
                }
                else if (m_toggleModeAlt)
                {
                    if (keyEvent.Keycode == m_toggleModeKeyCode && keyEvent.AltPressed)
                        ToggleModes();
                }
                else
                {
                    if (keyEvent.Keycode == m_toggleModeKeyCode)
                        ToggleModes();
                }

                // Toggle Active -------------------------------------
                if (m_toggleActiveCtrl && m_toggleActiveAlt)
                {
                    if (keyEvent.Keycode == m_toggleActiveKeyCode && keyEvent.AltPressed && keyEvent.IsCommandOrControlPressed())
                    {
                        ToggleActive();
                    }
                }

                else if (m_toggleActiveCtrl)
                {
                    if (keyEvent.Keycode == m_toggleActiveKeyCode && keyEvent.IsCommandOrControlPressed())
                    {
                        ToggleActive();
                    }
                }
                else if (m_toggleActiveAlt)
                {
                    if (keyEvent.Keycode == m_toggleActiveKeyCode && keyEvent.AltPressed)
                    {
                        ToggleActive();
                    }
                }
                else
                {
                    if (keyEvent.Keycode == m_toggleActiveKeyCode)
                    {
                        ToggleActive();
                    }
                }
            }
        }

        private void UpdateAllParameters()
        {
            m_fpsManager.UpdateParameters();
            m_ramManager.UpdateParameters();
            // m_audioManager.UpdateParameters();
            // m_advancedData.UpdateParameters();
        }

        private void RefreshAllParameters()
        {
            m_fpsManager.RefreshParameters();
            m_ramManager.RefreshParameters();
            // m_audioManager.RefreshParameters();
            // m_advancedData.RefreshParameters();
        }

        #endregion
    }
}