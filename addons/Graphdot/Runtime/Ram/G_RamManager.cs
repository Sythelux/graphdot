using System;
using System.Collections.Generic;
using Godot;
using Graphdot.Runtime.Util;

namespace Graphdot.Runtime.Ram
{
    ///<author email="dersyth@gmail.com">Sythelux Rikd</author>
    public partial class G_RamManager : Node
    {
        /* ----- TODO: ----------------------------
 * Add summaries to the variables.
 * Add summaries to the functions.
 * Check if we should add a "RequireComponent" for "RectTransform".
 * Check if we should add a "RequireComponent" for "RamGraph".
 * Check why this manager doesnt use RamMonitor, as all the other managers have a monitor script.
 * Check if we should add a "RequireComponent" for "RamText".
 * --------------------------------------*/

        #region Variables -> Serialized Private

        [Export]
        private Node m_ramGraphGameObject = null;

        [Export]
        private Panel[] m_backgroundImages = Array.Empty<Panel>();

        #endregion

        #region Variables -> Private

        private GraphdotManager m_GraphdotManager = null;

        private G_RamGraph m_ramGraph = null;
        private G_RamMonitor m_ramMonitor = null;
        private G_RamText m_ramText = null;

        private Control m_rectTransform = null;

        private List<Node> m_childrenGameObjects = new List<Node>();

        private GraphdotManager.ModuleState m_previousModuleState = GraphdotManager.ModuleState.FULL;
        private GraphdotManager.ModuleState m_currentModuleState = GraphdotManager.ModuleState.FULL;

        #endregion

        #region Methods -> Unity Callbacks

        public override void _EnterTree()
        {
            Init();
        }

        public override void _Ready()
        {
            UpdateParameters();
        }

        #endregion

        #region Methods -> Public

        public void SetPosition(GraphdotManager.ModulePosition newModulePosition)
        {
            float xSideOffset = Mathf.Abs(m_rectTransform.AnchorLeft);
            float ySideOffset = Mathf.Abs(m_rectTransform.AnchorTop);

            switch (newModulePosition)
            {
                case GraphdotManager.ModulePosition.TOP_LEFT:

                    // m_rectTransform.anchorMax = Vector2.up;
                    // m_rectTransform.anchorMin = Vector2.up;
                    // m_rectTransform.anchoredPosition = new Vector2(xSideOffset, -ySideOffset);

                    break;

                case GraphdotManager.ModulePosition.TOP_RIGHT:

                    // m_rectTransform.anchorMax = Vector2.one;
                    // m_rectTransform.anchorMin = Vector2.one;
                    // m_rectTransform.anchoredPosition = new Vector2(-xSideOffset, -ySideOffset);

                    break;

                case GraphdotManager.ModulePosition.BOTTOM_LEFT:

                    // m_rectTransform.anchorMax = Vector2.zero;
                    // m_rectTransform.anchorMin = Vector2.zero;
                    // m_rectTransform.anchoredPosition = new Vector2(xSideOffset, ySideOffset);

                    break;

                case GraphdotManager.ModulePosition.BOTTOM_RIGHT:

                    // m_rectTransform.anchorMax = Vector2.right;
                    // m_rectTransform.anchorMin = Vector2.right;
                    // m_rectTransform.anchoredPosition = new Vector2(-xSideOffset, ySideOffset);

                    break;

                case GraphdotManager.ModulePosition.FREE:
                    break;
            }
        }

        public void SetState(GraphdotManager.ModuleState state, bool silentUpdate = false)
        {
            if (!silentUpdate)
            {
                m_previousModuleState = m_currentModuleState;
            }

            m_currentModuleState = state;

            switch (state)
            {
                case GraphdotManager.ModuleState.FULL:
                    SetProcess(true); //SetActive(true);
                    m_childrenGameObjects.SetAllActive(true);
                    SetGraphActive(true);

                    // if (m_GraphdotManager.Background)
                    // {
                    //     m_backgroundImages.SetOneActive(0);
                    // }
                    // else
                    // {
                    //     m_backgroundImages.SetAllActive(false);
                    // }

                    break;

                case GraphdotManager.ModuleState.TEXT:
                case GraphdotManager.ModuleState.BASIC:
                    SetProcess(true); //SetActive(true);
                    m_childrenGameObjects.SetAllActive(true);
                    SetGraphActive(false);

                    // if (m_GraphdotManager.Background)
                    // {
                    //     m_backgroundImages.SetOneActive(1);
                    // }
                    // else
                    // {
                    //     m_backgroundImages.SetAllActive(false);
                    // }

                    break;

                case GraphdotManager.ModuleState.BACKGROUND:
                    SetProcess(true); //SetActive(true);
                    SetGraphActive(false);

                    m_childrenGameObjects.SetAllActive(false);
                    // m_backgroundImages.SetAllActive(false);

                    break;

                case GraphdotManager.ModuleState.OFF:
                    SetProcess(false); //SetActive(false);
                    break;
            }
        }

        public void RestorePreviousState()
        {
            SetState(m_previousModuleState);
        }

        public void UpdateParameters()
        {
            foreach (var image in m_backgroundImages)
            {
                image.AddThemeColorOverride("Background", m_GraphdotManager.BackgroundColor);
            }

            m_ramGraph.UpdateParameters();
            m_ramText.UpdateParameters();

            SetState(m_GraphdotManager.RamModuleState);
        }

        public void RefreshParameters()
        {
            foreach (var image in m_backgroundImages)
            {
                image.AddThemeColorOverride("Background", m_GraphdotManager.BackgroundColor);
            }

            m_ramGraph.UpdateParameters();
            m_ramText.UpdateParameters();

            SetState(m_currentModuleState, true);
        }

        #endregion

        #region Methods -> Private

        private void Init()
        {
            m_GraphdotManager = GetTree().Root.GetNode<GraphdotManager>();

            m_ramMonitor = this.GetNode<G_RamMonitor>();

            m_ramGraph = this.GetNode<G_RamGraph>();
            m_ramGraph.RamMonitor = m_ramMonitor;
            m_ramText = this.GetNode<G_RamText>();
            m_ramText.GraphdotManager = m_GraphdotManager;

            m_rectTransform = this.GetNode<Control>();

            // foreach (Transform child in transform)
            // {
            //     if (child.parent == transform)
            //     {
            //         m_childrenGameObjects.Add(child.gameObject);
            //     }
            // }
        }

        private void SetGraphActive(bool active)
        {
            m_ramGraph.SetProcess(active);
            // m_ramGraphGameObject.SetActive(active);
        }

        #endregion
    }
}