using Godot;
using Graphdot.Runtime.Util;

namespace Graphdot.Runtime.Ram
{
    ///<author email="dersyth@gmail.com">Sythelux Rikd</author>
    public partial class G_RamText : Control
    {
        #region Variables -> Serialized Private

        [Export]
        private RichTextLabel m_staticusageSystemMemorySizeText = null;

        [Export]
        private RichTextLabel m_dynamicusageSystemMemorySizeText = null;

        [Export]
        private RichTextLabel m_staticpeakusageSystemMemorySizeText = null;

        #endregion

        #region Variables -> Private

        private GraphdotManager m_GraphdotManager = null;

        private G_RamMonitor m_ramMonitor = null;

        private float m_updateRate = 4f; // 4 updates per sec.

        private double m_deltaTime = 0.0f;

        public GraphdotManager GraphdotManager
        {
            get => m_GraphdotManager;
            set => m_GraphdotManager = value;
        }

        #endregion

        #region Methods -> Unity Callbacks

        public override void _Ready()
        {
            Init();
        }

        public override void _Process(double delta)
        {
            m_deltaTime += delta; //TODO: unscaled time

            if (m_deltaTime > 1f / m_updateRate)
            {
                // Update staticusage, staticpeakusage and dynamicusage memory
                m_staticusageSystemMemorySizeText.Text = ((int)m_ramMonitor.StaticMemoryUsage).ToStringNonAlloc();
                m_dynamicusageSystemMemorySizeText.Text = ((int)m_ramMonitor.DynamicMemoryUsage).ToStringNonAlloc();
                m_staticpeakusageSystemMemorySizeText.Text = ((int)m_ramMonitor.StaticMemoryPeakUsage).ToStringNonAlloc();

                m_deltaTime = 0f;
            }
        }

        #endregion

        #region Methods -> Public

        public void UpdateParameters()
        {
            m_staticusageSystemMemorySizeText.Set("custom_colors/default_color", m_GraphdotManager.StaticMemoryUsageColor);
            m_dynamicusageSystemMemorySizeText.Set("custom_colors/default_color", m_GraphdotManager.DynamicMemoryUsageColor);
            m_staticpeakusageSystemMemorySizeText.Set("custom_colors/default_color", m_GraphdotManager.StaticMemoryPeakUsageColor);

            m_updateRate = m_GraphdotManager.RamTextUpdateRate;
        }

        #endregion

        #region Methods -> Private

        private void Init()
        {
            // We assume no game will consume more than 16GB of RAM.
            // If it does, who cares about some minuscule garbage allocation lol.
            G_IntString.Init(0, 16386);

            m_ramMonitor = GetParent().GetParent().GetNode<G_RamMonitor>();
            m_dynamicusageSystemMemorySizeText = G_ExtensionMethods.GetNode<RichTextLabel>(this, "dynamicusage_system_memory_size_text");
            m_staticusageSystemMemorySizeText = G_ExtensionMethods.GetNode<RichTextLabel>(this, "staticusage_system_memory_size_text");
            m_staticpeakusageSystemMemorySizeText = G_ExtensionMethods.GetNode<RichTextLabel>(this, "staticpeakusage_system_memory_size_text");

            UpdateParameters();
        }

        #endregion
    }
}