using Godot;

namespace Graphdot.Runtime.Ram
{
    ///<author email="dersyth@gmail.com">Sythelux Rikd</author>
    public partial class G_RamMonitor : Node
    {
        #region Properties -> Public

        public float StaticMemoryUsage { get; private set; }
        public float DynamicMemoryUsage { get; private set; }
        public float StaticMemoryPeakUsage { get; private set; }

        #endregion

        #region Methods -> Unity Callbacks

        public override void _Process(double delta)
        {
            StaticMemoryUsage = OS.GetStaticMemoryUsage() / 1048576f;
            // DynamicMemoryUsage = OS.Get() / 1048576f;
            StaticMemoryPeakUsage = OS.GetStaticMemoryPeakUsage() / 1048576f;
        }

        #endregion
    }
}