using Godot;
using System;
using Graphdot.Runtime;
using Graphdot.Runtime.Shaders;
using Graphdot.Runtime.Util;

namespace Graphdot.Runtime.Ram
{
    public partial class G_RamGraph : Control
    {
        #region Variables -> Serialized Private

        [Export]
        private TextureRect m_imageStaticUsage = null;

        [Export]
        private TextureRect m_imageDynamicUsage = null;

        [Export]
        private TextureRect m_imageStaticPeakUsage = null;

        [Export]
        private Shader ShaderFull = null;

        [Export]
        private Shader ShaderLight = null;

        [Export]
        private bool m_isInitialized = false;

        #endregion

        #region Variables -> Private

        private GraphdotManager m_GraphdotManager = null;

        private G_RamMonitor m_ramMonitor = null;

        private int m_resolution = 150;

        private G_GraphShader m_shaderGraphStaticUsage = null;
        private G_GraphShader m_shaderGraphDynamicUsage = null;
        private G_GraphShader m_shaderGraphStaticPeakUsage = null;

        private float[] m_staticusageArray;
        private float[] m_dynamicusageArray;
        private float[] m_staticpeakusageArray;

        private float m_highestStaticMemory = 0;
        private float m_highestDynamicMemory = 0;

        public G_RamMonitor RamMonitor
        {
            get => m_ramMonitor;
            set => m_ramMonitor = value;
        }
        #endregion

        #region Methods -> Unity Callbacks

        public override void _Process(double delta)
        {
            UpdateGraph();
        }

        #endregion

        #region Methods -> Public

        public void UpdateParameters()
        {
            if (m_shaderGraphStaticUsage == null
                || m_shaderGraphDynamicUsage == null
                || m_shaderGraphStaticPeakUsage == null)
            {
                /*
                 * Note: this is fine, since we don't much
                 * care what granularity we use if the graph
                 * has not been initialized, i.e. it's disabled.
                 * There is no chance that for some reason 
                 * parameters will not stay up to date if
                 * at some point in the future the graph is enabled:
                 * at the end of Init(), UpdateParameters() is
                 * called again.
                 */
                return;
            }

            switch (m_GraphdotManager.GraphdotMode)
            {
                case GraphdotManager.Mode.FULL:
                    m_shaderGraphStaticUsage.ArrayMaxSize = G_GraphShader.ArrayMaxSizeFull;
                    m_shaderGraphDynamicUsage.ArrayMaxSize = G_GraphShader.ArrayMaxSizeFull;
                    m_shaderGraphStaticPeakUsage.ArrayMaxSize = G_GraphShader.ArrayMaxSizeFull;

                    // m_shaderGraphStaticUsage.Image.material = new Material(ShaderFull);
                    // m_shaderGraphDynamicUsage.Image.material = new Material(ShaderFull);
                    // m_shaderGraphStaticPeakUsage.Image.material = new Material(ShaderFull);
                    break;

                case GraphdotManager.Mode.LIGHT:
                    m_shaderGraphStaticUsage.ArrayMaxSize = G_GraphShader.ArrayMaxSizeLight;
                    m_shaderGraphDynamicUsage.ArrayMaxSize = G_GraphShader.ArrayMaxSizeLight;
                    m_shaderGraphStaticPeakUsage.ArrayMaxSize = G_GraphShader.ArrayMaxSizeLight;

                    // m_shaderGraphStaticUsage.Image.material = new Material(ShaderLight);
                    // m_shaderGraphDynamicUsage.Image.material = new Material(ShaderLight);
                    // m_shaderGraphStaticPeakUsage.Image.material = new Material(ShaderLight);
                    break;
            }

            m_shaderGraphStaticUsage.InitializeShader();
            m_shaderGraphDynamicUsage.InitializeShader();
            m_shaderGraphStaticPeakUsage.InitializeShader();

            m_resolution = m_GraphdotManager.RamGraphResolution;

            CreatePoints();
        }

        #endregion

        #region Methods -> Protected Override

        protected void UpdateGraph()
        {
            // Since we no longer initialize by default OnEnable(), 
            // we need to check here, and Init() if needed
            if (!m_isInitialized)
            {
                Init();
            }

            float staticusageMemory = m_ramMonitor.StaticMemoryUsage;
            float dynamicusageMemory = m_ramMonitor.DynamicMemoryUsage;
            float staticpeakusageMemory = m_ramMonitor.StaticMemoryPeakUsage;

            m_highestStaticMemory = 0;

            for (int i = 0; i <= m_resolution - 1; i++)
            {
                if (i >= m_resolution - 1)
                {
                    m_staticusageArray[i] = staticusageMemory;
                    m_dynamicusageArray[i] = dynamicusageMemory;
                    m_staticpeakusageArray[i] = staticpeakusageMemory;
                }
                else
                {
                    m_staticusageArray[i] = m_staticusageArray[i + 1];
                    m_dynamicusageArray[i] = m_dynamicusageArray[i + 1];
                    m_staticpeakusageArray[i] = m_staticpeakusageArray[i + 1];
                }

                if (m_highestStaticMemory < m_staticpeakusageArray[i])
                    m_highestStaticMemory = m_staticpeakusageArray[i];
                if (m_highestDynamicMemory < m_dynamicusageArray[i])
                    m_highestDynamicMemory = m_dynamicusageArray[i];
            }

            for (int i = 0; i <= m_resolution - 1; i++)
            {
                m_shaderGraphStaticUsage.ShaderArrayValues[i] = m_staticusageArray[i] / m_highestStaticMemory;
                m_shaderGraphDynamicUsage.ShaderArrayValues[i] = m_dynamicusageArray[i] / m_highestDynamicMemory;
                m_shaderGraphStaticPeakUsage.ShaderArrayValues[i] = m_staticpeakusageArray[i] / m_highestStaticMemory;
            }

            m_shaderGraphStaticUsage.UpdatePoints();
            m_shaderGraphDynamicUsage.UpdatePoints();
            m_shaderGraphStaticPeakUsage.UpdatePoints();
        }

        protected void CreatePoints()
        {
            if (m_shaderGraphStaticUsage.ShaderArrayValues == null || m_shaderGraphStaticUsage.ShaderArrayValues.Length != m_resolution)
            {
                m_staticusageArray = new float[m_resolution];
                m_dynamicusageArray = new float[m_resolution];
                m_staticpeakusageArray = new float[m_resolution];

                m_shaderGraphStaticUsage.ShaderArrayValues = new float[m_resolution];
                m_shaderGraphDynamicUsage.ShaderArrayValues = new float[m_resolution];
                m_shaderGraphStaticPeakUsage.ShaderArrayValues = new float[m_resolution];
            }

            for (int i = 0; i < m_resolution; i++)
            {
                m_shaderGraphStaticUsage.ShaderArrayValues[i] = 0;
                m_shaderGraphDynamicUsage.ShaderArrayValues[i] = 0;
                m_shaderGraphStaticPeakUsage.ShaderArrayValues[i] = 0;
            }

            // Initialize the material values

            // Colors

            m_shaderGraphStaticUsage.GoodColor = m_GraphdotManager.StaticMemoryUsageColor;
            m_shaderGraphStaticUsage.CautionColor = m_GraphdotManager.StaticMemoryUsageColor;
            m_shaderGraphStaticUsage.CriticalColor = m_GraphdotManager.StaticMemoryUsageColor;

            m_shaderGraphStaticUsage.UpdateColors();

            m_shaderGraphDynamicUsage.GoodColor = m_GraphdotManager.DynamicMemoryUsageColor;
            m_shaderGraphDynamicUsage.CautionColor = m_GraphdotManager.DynamicMemoryUsageColor;
            m_shaderGraphDynamicUsage.CriticalColor = m_GraphdotManager.DynamicMemoryUsageColor;

            m_shaderGraphDynamicUsage.UpdateColors();

            m_shaderGraphStaticPeakUsage.GoodColor = m_GraphdotManager.StaticMemoryPeakUsageColor;
            m_shaderGraphStaticPeakUsage.CautionColor = m_GraphdotManager.StaticMemoryPeakUsageColor;
            m_shaderGraphStaticPeakUsage.CriticalColor = m_GraphdotManager.StaticMemoryPeakUsageColor;

            m_shaderGraphStaticPeakUsage.UpdateColors();

            // Thresholds

            m_shaderGraphStaticUsage.GoodThreshold = 0;
            m_shaderGraphStaticUsage.CautionThreshold = 0;
            m_shaderGraphStaticUsage.UpdateThresholds();

            m_shaderGraphDynamicUsage.GoodThreshold = 0;
            m_shaderGraphDynamicUsage.CautionThreshold = 0;
            m_shaderGraphDynamicUsage.UpdateThresholds();

            m_shaderGraphStaticPeakUsage.GoodThreshold = 0;
            m_shaderGraphStaticPeakUsage.CautionThreshold = 0;
            m_shaderGraphStaticPeakUsage.UpdateThresholds();

            m_shaderGraphStaticUsage.UpdateArray();
            m_shaderGraphDynamicUsage.UpdateArray();
            m_shaderGraphStaticPeakUsage.UpdateArray();

            // Average

            m_shaderGraphStaticUsage.Average = 0;
            m_shaderGraphDynamicUsage.Average = 0;
            m_shaderGraphStaticPeakUsage.Average = 0;

            m_shaderGraphStaticUsage.UpdateAverage();
            m_shaderGraphDynamicUsage.UpdateAverage();
            m_shaderGraphStaticPeakUsage.UpdateAverage();
        }

        #endregion

        #region Methods -> Private

        private void Init()
        {
            m_GraphdotManager = GetTree().Root.GetNode<GraphdotManager>();

            // m_ramMonitor = GetComponent<G_RamMonitor>();

            m_imageStaticUsage = GetNode<TextureRect>("ram_staticusage_graph");
            m_imageDynamicUsage = GetNode<TextureRect>("ram_dynamicusage_graph");
            m_imageStaticPeakUsage = GetNode<TextureRect>("ram_staticpeakusage_graph");

            m_shaderGraphStaticUsage = new G_GraphShader
            {
                Image = m_imageStaticUsage
            };
            m_shaderGraphDynamicUsage = new G_GraphShader
            {
                Image = m_imageDynamicUsage
            };
            m_shaderGraphStaticPeakUsage = new G_GraphShader
            {
                Image = m_imageStaticPeakUsage
            };

            // m_shaderGraphStaticUsage.Image = m_imageStaticUsage;
            // m_shaderGraphDynamicUsage.Image = m_imageDynamicUsage;
            // m_shaderGraphStaticPeakUsage.Image = m_imageStaticPeakUsage;

            UpdateParameters();

            m_isInitialized = true;
        }

        #endregion
    }
}