using System.Collections.Generic;
using System.Linq;
using Godot;

namespace Graphdot.Runtime.Util
{
    public static class G_ExtensionMethods
    {
        #region Methods -> Extension Methods

        /// <summary>
        /// Functions as the SetActive function in the GameObject class, but for a list of them.
        /// </summary>
        /// <param name="gameObjects">
        /// List of GameObjects.
        /// </param>
        /// <param name="active">
        /// Wether to turn them on or off.
        /// </param>
        public static IEnumerable<Node> SetAllActive(this IEnumerable<Node> gameObjects, bool active)
        {
            foreach (Node gameObj in gameObjects)
            {
                gameObj.SetProcess(active);
                yield return gameObj;
            }
        }

        // public static List<TextureRect> SetOneActive(this List<TextureRect> images, int active)
        // {
        //     for (int i = 0; i < images.Count; i++)
        //     {
        //         images[i].SetProcess(i == active);
        //     }
        //
        //     return images;
        // }
        //
        // public static List<TextureRect> SetAllActive(this List<TextureRect> images, bool active)
        // {
        //     foreach (var image in images)
        //     {
        //         image.gameObject.SetActive(active);
        //     }
        //
        //     return images;
        // }

        public static void SetShaderParameter(this Material material, string name, Variant value) => material.Set($"shader_param/{name}", value);

        public static T GetNode<T>(this Node node, NodePath search = null) where T : Node => node.GetNodes<T>(search).FirstOrDefault();

        public static IEnumerable<T> GetNodes<T>(this Node node, NodePath search = null) where T : Node
        {
            if (node is T t && (search == null || string.Equals(t.Name, search)))
                yield return t;

            for (var i = 0; i < node.GetChildCount(); i++)
            {
                var child = node.GetChild(i);
                if (child is T t1 && (search == null || string.Equals(t1.Name, search)))
                    yield return t1;
            }

            for (var i = 0; i < node.GetChildCount(); i++)
                foreach (T t3 in node.GetChild(i).GetNodes<T>(search))
                    yield return t3;
        }

        public static void GetOutputData(this AudioEffectSpectrumAnalyzerInstance instance, float[] samples, int channel)
        {
            var sampleRate = AudioServer.GetMixRate();
            for (var i = 0; i < samples.Length; i++)
                samples[i] = instance.GetMagnitudeForFrequencyRange(sampleRate / samples.Length * i, sampleRate / samples.Length * (i + 1)).Y;
        }

        public static void GetSpectrumData(this AudioEffectSpectrumAnalyzerInstance instance, float[] samples, int channel, FFTWindow window)
        {
            var sampleRate = AudioServer.GetMixRate();
            for (var i = 0; i < samples.Length; i++)
                samples[i] = window.Get(instance.GetMagnitudeForFrequencyRange(sampleRate / samples.Length * i, sampleRate / samples.Length * (i + 1)).Y);
        }

        #endregion
    }
}