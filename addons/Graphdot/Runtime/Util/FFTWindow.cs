using System;
using Godot;

namespace Graphdot.Runtime.Util
{
    ///<author email="a.schaub@lefx.de">Aron Schaub</author>
    [Serializable]
    public abstract class FFTWindow
    {
        public static readonly float N = 64;

        public static FFTWindow Blackman = new BlackmanFFTWindow();

        public abstract float Get(float f);
    }

    class BlackmanFFTWindow : FFTWindow
    {
        public override float Get(float n)
        {
            return 0.42f - (0.5f * Mathf.Cos(n / N)) + (0.08f * Mathf.Cos(2.0f * n / N));
        }
    }
}