using System;
using System.Diagnostics;
using Godot;

namespace Graphdot.Runtime.Util
{
    public partial class G_Singleton<T> : Node where T : Node
    {
        #region Variables -> Private

        private static T _instance;

        private static object _lock = new object();

        #endregion

        #region Properties -> Public

        public static T Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        Console.WriteLine
                        (
                            "[Singleton] An instance of " + typeof(T) +
                            " is trying to be accessed, but it wasn't initialized first. " +
                            "Make sure to add an instance of " + typeof(T) + " in the scene before " +
                            " trying to access it."
                        );
                    }

                    return _instance;
                }
            }
        }

        #endregion

        #region Methods -> Unity Callbacks

        public override void _Ready()
        {
            if (_instance != null)
            {
                QueueFree();
            }
            else
            {
                _instance = this as T;
            }
        }

        public override void _Process(double delta)
        {
            if (_instance == this)
            {
                _instance = null;
            }
        }

        #endregion
    }
}