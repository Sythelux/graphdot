using Godot;
using Graphdot.Runtime.Util;

namespace Graphdot.Runtime.Shaders
{
    public partial class G_GraphShader
    {
        #region Variables

        public const int ArrayMaxSizeFull = 512;
        public const int ArrayMaxSizeLight = 128;

        public int ArrayMaxSize = 128;

        public Image _shaderArrayImg;
        public float[] _shaderArrayValues;

        public float[] ShaderArrayValues
        {
            get => _shaderArrayValues;
            set
            {
                _shaderArrayValues = value;
                _shaderArrayImg = Godot.Image.Create(value.Length, 1, false, Godot.Image.Format.Rf);
                _shaderArrayTexture = ImageTexture.CreateFromImage(_shaderArrayImg);
            }
        }


        public TextureRect Image = null;


        private string Name = "GraphValues"; // The name of the array
        private string Name_Length = "GraphValues_Length";


        public float Average = 0;
        private string m_averagePropertyId = "Average";

        public float GoodThreshold = 0;
        public float CautionThreshold = 0;

        private string m_goodThresholdPropertyId = "Good_Threshold";
        private string m_cautionThresholdPropertyId = "Caution_Threshold";


        public Color GoodColor = Colors.White;
        public Color CautionColor = Colors.White;
        public Color CriticalColor = Colors.White;

        private string m_goodColorPropertyId = "Good_Color";
        private string m_cautionColorPropertyId = "Caution_Color";
        private string m_criticalColorPropertyId = "Critical_Color";
        private ImageTexture _shaderArrayTexture;

        #endregion

        #region Methods -> Public

        /// <summary>
        /// This is done to avoid a design problem that arrays in shaders have, 
        /// and should be called before initializing any shader graph.
        /// The first time that you use initialize an array, the size of the array in the shader is fixed.
        /// This is why sometimes you will get a warning saying that the array size will be capped.
        /// It shouldn't generate any issues, but in the worst case scenario just reset the Unity Editor
        /// (if for some reason the shaders reload).
        /// I also cache the Property IDs, that make access faster to modify shader parameters.
        /// </summary>
        public void InitializeShader()
        {
            // Image.Material.SetShaderParam(Name, im);
        }

        /// <summary>
        /// Updates the material linked with this shader graph  with the values in the float[] array.
        /// </summary>
        public void UpdateArray()
        {
            Image.Material.SetShaderParameter(Name_Length, _shaderArrayValues.Length);
        }

        /// <summary>
        /// Updates the average parameter in the material.
        /// </summary>
        public void UpdateAverage()
        {
            Image.Material.SetShaderParameter(m_averagePropertyId, Average);
        }

        /// <summary>
        /// Updates the thresholds in the material.
        /// </summary>
        public void UpdateThresholds()
        {
            Image.Material.SetShaderParameter(m_goodThresholdPropertyId, GoodThreshold);
            Image.Material.SetShaderParameter(m_cautionThresholdPropertyId, CautionThreshold);
        }

        /// <summary>
        /// Updates the colors in the material.
        /// </summary>
        public void UpdateColors()
        {
            Image.Material.SetShaderParameter(m_goodColorPropertyId, GoodColor);
            Image.Material.SetShaderParameter(m_cautionColorPropertyId, CautionColor);
            Image.Material.SetShaderParameter(m_criticalColorPropertyId, CriticalColor);
        }

        /// <summary>
        /// Updates the points in the graph with the set array of values.
        /// </summary>
        public void UpdatePoints()
        {
            // Requires an array called "name"
            // and another one called "name_Length"

            for (var i = 0; i < _shaderArrayValues.Length; i++)
                _shaderArrayImg.SetPixel(i, 0, new Color(_shaderArrayValues[i], 0, 0));
            _shaderArrayTexture.SetImage(_shaderArrayImg);

            Image.Material.SetShaderParameter(Name, _shaderArrayTexture);
        }

        #endregion
    }
}