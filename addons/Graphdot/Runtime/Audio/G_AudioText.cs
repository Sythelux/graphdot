/* ---------------------------------------
 * Original Work by:
 * Author:          Martin Pane (martintayx@gmail.com) (@tayx94)
 * Contributors:    https://github.com/Tayx94/graphy/graphs/contributors
 * Project:         Graphy - Ultimate Stats Monitor
 * Date:            15-Dec-17
 * Studio:          Tayx
 *
 * Git repo:        https://github.com/Tayx94/graphy
 *
 * This project is released under the MIT license.
 * Attribution is not required, but it is always welcomed!
 * -------------------------------------*/

using Godot;
using Graphdot.Runtime.Util;

namespace Graphdot.Runtime.Audio
{
    ///<author email="dersyth@gmail.com">Sythelux Rikd</author>
    public partial class G_AudioText : Control
    {
        #region Variables -> Serialized Private

        [Export]
        private RichTextLabel m_DBText = null;

        #endregion

        #region Variables -> Private

        private GraphdotManager m_GraphdotManager = null;

        private G_AudioMonitor m_audioMonitor = null;

        private int m_updateRate = 4;

        private double m_deltaTimeOffset = 0;

        public GraphdotManager GraphdotManager
        {
            get => m_GraphdotManager;
            set => m_GraphdotManager = value;
        }

        #endregion

        #region Methods -> Unity Callbacks

        public override void _Ready()
        {
            Init();
        }

        public override void _Process(double delta)
        {
            if (m_audioMonitor.SpectrumDataAvailable)
            {
                if (m_deltaTimeOffset > 1f / m_updateRate)
                {
                    m_deltaTimeOffset = 0f;

                    m_DBText.Text = Mathf.Clamp((int)m_audioMonitor.MaxDB, -80, 0).ToStringNonAlloc();
                }
                else
                {
                    m_deltaTimeOffset += delta;
                }
            }
        }

        #endregion

        #region Methods -> Public

        public void UpdateParameters()
        {
            m_updateRate = m_GraphdotManager.AudioTextUpdateRate;
        }

        #endregion

        #region Methods -> Private

        private void Init()
        {
            G_IntString.Init(-80, 0); // dB range

            m_audioMonitor = GetParent().GetParent().GetNode<G_AudioMonitor>();
            
            m_DBText = G_ExtensionMethods.GetNode<RichTextLabel>(this, "db_value_text");

            UpdateParameters();
        }

        #endregion
    }
}