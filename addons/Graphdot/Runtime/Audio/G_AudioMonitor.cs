/* ---------------------------------------
 * Original Work by:
 * Author:          Martin Pane (martintayx@gmail.com) (@tayx94)
 * Contributors:    https://github.com/Tayx94/graphy/graphs/contributors
 * Project:         Graphy - Ultimate Stats Monitor
 * Date:            15-Dec-17
 * Studio:          Tayx
 *
 * Git repo:        https://github.com/Tayx94/graphy
 *
 * This project is released under the MIT license.
 * Attribution is not required, but it is always welcomed!
 * -------------------------------------*/

using Godot;
using Graphdot.Runtime.Util;

namespace Graphdot.Runtime.Audio
{
    ///<author email="dersyth@gmail.com">Sythelux Rikd</author>
    public partial class G_AudioMonitor : Node
    {
        #region Variables -> Private

        private const float m_refValue = 1f;

        private GraphdotManager m_GraphdotManager = null;

        private AudioEffectSpectrumAnalyzerInstance m_audioEffectInstance = null;

        private GraphdotManager.LookForAudioListener m_findAudioListenerInCameraIfNull = GraphdotManager.LookForAudioListener.ON_SCENE_LOAD;

        private FFTWindow m_FFTWindow = FFTWindow.Blackman;

        private int m_spectrumSize = 512;

        #endregion

        #region Properties -> Public

        /// <summary>
        /// Current audio spectrum from the specified AudioListener.
        /// </summary>
        public float[] Spectrum { get; private set; }

        /// <summary>
        /// Highest audio spectrum from the specified AudioListener in the last few seconds.
        /// </summary>
        public float[] SpectrumHighestValues { get; private set; }

        /// <summary>
        /// Maximum DB registered in the current spectrum.
        /// </summary>
        public float MaxDB { get; private set; }

        /// <summary>
        /// Returns true if there is a reference to the audio listener.
        /// </summary>
        public bool SpectrumDataAvailable => m_audioEffectInstance != null;

        #endregion

        #region Methods -> Unity Callbacks

        public override void _Ready()
        {
            Init();
        }

        /**
         * TODO:
         * extends Node2D

            const VU_COUNT = 16
            const FREQ_MAX = 11050.0

            const WIDTH = 400
            const HEIGHT = 100

            const MIN_DB = 60

            var spectrum

            func _draw():
	            #warning-ignore:integer_division
	            var w = WIDTH / VU_COUNT
	            var prev_hz = 0
	            for i in range(1, VU_COUNT+1):
		            var hz = i * FREQ_MAX / VU_COUNT;
		            var magnitude: float = spectrum.get_magnitude_for_frequency_range(prev_hz, hz).length()
		            var energy = clamp((MIN_DB + linear2db(magnitude)) / MIN_DB, 0, 1)
		            var height = energy * HEIGHT
		            draw_rect(Rect2(w * i, HEIGHT - height, w, height), Color.white)
		            prev_hz = hz


            func _process(_delta):
	            update()


            func _ready():
	            spectrum = AudioServer.get_bus_effect_instance(0,0)

         */
        
        public override void _Process(double delta)
        {
            if (m_audioEffectInstance != null)
            {
                // Use this data to calculate the dB value

                // AudioListener.GetOutputData(Spectrum, 0);
                // Spectrum = AudioServer.GetBusEffectInstance(0, 0).GetMagnitudeForFrequencyRange(0, 1);
                m_audioEffectInstance.GetOutputData(Spectrum, 0);

                float sum = 0;

                for (int i = 0; i < Spectrum.Length; i++)
                {
                    sum += Spectrum[i] * Spectrum[i]; // sum squared samples
                }

                float rmsValue = Mathf.Sqrt(sum / Spectrum.Length); // rms = square root of average

                MaxDB = 20 * Mathf.Log(rmsValue / m_refValue); // calculate dB

                // if (MaxDB < -80) MaxDB = -80; // clamp it to -80dB min

                // Use this data to draw the spectrum in the graphs

                // AudioListener.GetSpectrumData(Spectrum, 0, m_FFTWindow);
                // m_audioEffectInstance.GetSpectrumData(Spectrum, 0, m_FFTWindow);


                for (int i = 0; i < Spectrum.Length; i++)
                {
                    // Update the highest value if its lower than the current one
                    if (Spectrum[i] > SpectrumHighestValues[i])
                    {
                        SpectrumHighestValues[i] = Spectrum[i];
                    }

                    // Slowly lower the value 
                    else
                    {
                        SpectrumHighestValues[i] = (float)Mathf.Clamp(SpectrumHighestValues[i] - SpectrumHighestValues[i] * delta * 2, 0, 1);
                    }
                }
            }
            else if (m_audioEffectInstance == null
                     && m_findAudioListenerInCameraIfNull == GraphdotManager.LookForAudioListener.ALWAYS)
            {
                m_audioEffectInstance = FindAudioListener();
            }
        }

        #endregion

        #region Methods -> Public

        public void UpdateParameters()
        {
            m_findAudioListenerInCameraIfNull = m_GraphdotManager.FindAudioListenerInCameraIfNull;

            m_audioEffectInstance = m_GraphdotManager.AudioListener;
            m_FFTWindow = m_GraphdotManager.FftWindow;
            m_spectrumSize = m_GraphdotManager.SpectrumSize;

            if (m_audioEffectInstance == null
                && m_findAudioListenerInCameraIfNull != GraphdotManager.LookForAudioListener.NEVER)
            {
                m_audioEffectInstance = FindAudioListener();
            }

            Spectrum = new float[m_spectrumSize];
            SpectrumHighestValues = new float[m_spectrumSize];
        }

        /// <summary>
        /// Converts spectrum values to decibels using logarithms.
        /// </summary>
        /// <param name="linear"></param>
        /// <returns></returns>
        public float lin2dB(float linear)
        {
            return Mathf.Clamp(Mathf.Log(linear) * 20.0f, -160.0f, 0.0f);
        }

        /// <summary>
        /// Normalizes a value in decibels between 0-1.
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public float dBNormalized(float db)
        {
            return (db + 160f) / 160f;
        }

        #endregion

        #region Methods -> Private

        /// <summary>
        /// Tries to find an audio listener in the main camera.
        /// </summary>
        private AudioEffectSpectrumAnalyzerInstance FindAudioListener()
        {
            for (var i = 0; i < AudioServer.GetBusEffectCount(0); i++)
            {
                m_audioEffectInstance = AudioServer.GetBusEffectInstance(0, i) as AudioEffectSpectrumAnalyzerInstance;
                if (m_audioEffectInstance != null)
                    break;
            }

            if (m_audioEffectInstance == null)
            {
                AudioServer.AddBusEffect(0, new AudioEffectSpectrumAnalyzer());
                m_audioEffectInstance = AudioServer.GetBusEffectInstance(0, 0) as AudioEffectSpectrumAnalyzerInstance;
            }

            return m_audioEffectInstance;
        }

        public override void _EnterTree()
        {
            if (m_findAudioListenerInCameraIfNull == GraphdotManager.LookForAudioListener.ON_SCENE_LOAD)
            {
                m_audioEffectInstance = FindAudioListener();
            }
        }

        private void Init()
        {
            m_GraphdotManager = GetTree().Root.GetNode<GraphdotManager>();

            UpdateParameters();
        }

        #endregion
    }
}